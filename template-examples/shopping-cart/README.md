# README #

## What is this repository for? ##

Sure the TodoMVC examples are nice, but they often only capture a small portion of what you can do with Templates.

This is an example that tackles some of the complex problems of templating languages such as eventing and nested dependencies, to give you a better idea of how you can use these languages to fulfill your needs.

## How do I get set up? ##

To see all of the examples at work go [here](http://chimon2000.bitbucket.org/template-examples/shopping-cart/dist): 

- Summary of set up      
	1. npm install
	2. bower install
	3. gulp build
	4. gulp webpack-dev
	5. Go to localhost:8080/dist in your browser
+ Library Examples
	* [Vue.js - MVVM Made Simple](http://vuejs.org) 
    * [knockout - A Simple Dynamic MVVM Library](http://knockoutjs.com)
    * [HTMLjs - A JavaScript Driven MVVM Library](http://nhanaswigs.github.io/htmljs/api/index.html)
+ Dependencies
    * [jQuery](http://jquery.com/)
    * [Webpack - A Module Bundler](https://webpack.github.io)
    * [gulp.js - The streaming build system](https://gulpjs.com)
+ Coming Soon(er) or Later
    * [Ractive.js](http://www.ractivejs.org)
    * [Flight](http://flightjs.github.io)
    * [React](http://facebook.github.io/react)
    * [Transparency](http://leonidas.github.io/transparency)
	
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact