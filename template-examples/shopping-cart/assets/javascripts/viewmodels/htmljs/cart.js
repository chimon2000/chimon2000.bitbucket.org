/**
 * Created by redge on 10/17/2014.
 */
//var html = require('htmljs');
var view = require('views/htmljs/cart.html');
var $ = require('jquery');

var template = $( view );

$('body').html(template);

// Class to represent a row in the seat reservations grid
function SeatReservation(name, initialMeal) {
    var self = this;
    self.name = html.data(name);
    self.meal = html.data(initialMeal);

    self.formattedPrice = html.data(function() {
        var price = self.meal().price;
        return price ? "$" + price.toFixed(2) : "None";
    });
}

// Overall viewmodel for this screen, along with initial state
function ReservationsViewModel() {
    var self = this;

    // Non-editable catalog data - would come from the server
    self.availableMeals = [
        { mealName: "Standard (sandwich)", price: 0 },
        { mealName: "Premium (lobster)", price: 34.95 },
        { mealName: "Ultimate (whole zebra)", price: 290 }
    ];

    // Editable data
    self.seats = html.data([
        new SeatReservation("Steve", self.availableMeals[0]),
        new SeatReservation("Bert", self.availableMeals[1])
    ]);

    // Computed data
    self.totalSurcharge = html.data(function() {
        var total = 0;
        for (var i = 0; i < self.seats().length; i++)
            total += self.seats()[i].meal().price;
        return total.toFixed(2);
    });

    self.totalDiscount = html.data(function() {
        return (self.totalSurcharge()*90/100).toFixed(2);
    });

    self.seatNum = html.data(function(){
        return self.seats().length;
    });

    // Operations
    self.addSeat = function() {
        self.seats.add(new SeatReservation("", self.availableMeals[0]));
    }
    self.removeSeat = function(e, seat) { self.seats.remove(seat) }
}
var vm = new ReservationsViewModel();

html('#meal-reservation table tbody').each(vm.seats, function(seat, index){
    html.tr()
        .td().input(seat.name).clss('form-control input-w160').$().$()
        .td()
        .dropdown(vm.availableMeals, seat.meal, 'mealName').clss('form-control input-w210 margin5').refresh(seat, vm).$()
        .$()
        .td().span(seat.formattedPrice).$().$()
        .td().button('Remove').clss('btnViolet').click(vm.removeSeat, seat).refresh(vm).$().$()
        .$()
});


//computed properties
html('#seatNumber').text(vm.seatNum);
html('#surcharge').text(vm.totalSurcharge);
html('#discount').text(vm.totalDiscount);

// event handling
html('#addSeat').click(vm.addSeat).refresh(vm);