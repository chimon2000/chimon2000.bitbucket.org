/**
 * Created by redge on 10/17/2014.
 */

var view = require('views/knockout/cart.html');
var ko = require('knockout');
var $ = require('jquery');

var template = $( view );
$('body').append(template);

var availableMeals = [
    { mealName: "Standard (sandwich)", price: 0 },
    { mealName: "Premium (lobster)", price: 34.95 },
    { mealName: "Ultimate (whole zebra)", price: 290 }
];

// Class to represent a row in the seat reservations grid
function SeatReservation(name, initialMeal) {
    var self = this;
    self.name = ko.observable(name);
    self.meal = ko.observable(initialMeal);
    self.selectedMealName = ko.observable(initialMeal.mealName);

    self.mealSelected = ko.computed(function(){
        availableMeals.some(function(single){
            if(single.mealName == self.selectedMealName){
                self.meal =single;
            }
            return single.mealName == self.selectedMealName;
        });
    });

    self.formattedPrice = ko.computed(function() {
        var price = self.meal().price;
        return price ? "$" + price.toFixed(2) : "None";
    });
}

// Overall viewmodel for this screen, along with initial state
function ReservationsViewModel() {
    var self = this;
    var seats = [
        new SeatReservation("Steve", availableMeals[0]),
        new SeatReservation("Bert", availableMeals[1])
    ];

//    seats.forEach(function(single){
//        single.meal.subscribe(function(newValue){
//            var total = 0;
//            total = self.seats().reduce(function(previous, currentSeat){
//                return previous + currentSeat.meal().price;
//            }, total);
//
//            self.totalSurcharge(total);
//        });
//    });


    // Editable data
    self.seats = ko.observableArray(seats);

    self.numSeats = ko.computed(function(){
        return  self.seats().length;
    });

    self.totalSurcharge = ko.observable();

//    var total = 0;
//    total = self.seats().reduce(function(previous, currentSeat){
//        return previous + currentSeat.meal().price;
//    }, total).toFixed(2);
//
//    self.totalSurcharge(total);

    self.totalSurcharge = ko.pureComputed(function() {
        var total = 0;
        total = self.seats().reduce(function(previous, currentSeat){
            return previous + currentSeat.meal().price;
        }, total);

        return total;
    });

    self.totalDiscount = ko.computed(function() {
        var totalSurcharge = parseInt(self.totalSurcharge());
        return (totalSurcharge*90/100).toFixed(2);
    });

    // Operations
    self.addSeat = function() {
        var newReservation = new SeatReservation("", availableMeals[0]);
        newReservation.meal.subscribe(function(newValue){
            var total = 0;
            total = self.seats().reduce(function(previous, currentSeat){
                return previous + currentSeat.meal().price;
            }, total);

            self.totalSurcharge(total);
        });
        self.seats.push(new SeatReservation("", availableMeals[0]));
    }
    self.removeSeat = function(item) {
        self.seats.remove(item)
    }

    return {
        availableMeals: ko.observableArray(availableMeals),
        totalSurcharge: self.totalSurcharge,
        seats: self.seats,
        numSeats: self.numSeats,
        totalDiscount: self.totalDiscount,
        addSeat: self.addSeat,
        removeSeat: self.removeSeat
    }
}

var vm = new ReservationsViewModel();

ko.applyBindings(vm, $('#ko-meal-reservation').get(0));