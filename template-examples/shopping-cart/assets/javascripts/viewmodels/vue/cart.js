/**
 * Created by redge on 10/17/2014.
 */

var view = require('views/vue/cart.html');
var Vue = require('vue');
var $ = require('jquery');

var template = $( view );
$('body').append(template);

var availableMeals = [
    { mealName: "Standard (sandwich)", price: 0 },
    { mealName: "Premium (lobster)", price: 34.95 },
    { mealName: "Ultimate (whole zebra)", price: 290 }
];

// Class to represent a row in the seat reservations grid
function SeatReservation(name, initialMeal) {
    var self = this;
    self.name = name;
    self.meal = initialMeal;
    self.selectedMealName = initialMeal.mealName;

    self.mealSelected = function(){
        availableMeals.some(function(single){
            if(single.mealName == self.selectedMealName){
                self.meal =single;
            }
            return single.mealName == self.selectedMealName;
        });
    }
    self.formattedPrice = function() {

        var price = self.meal.price;
        return price ? "$" + price.toFixed(2) : "None";
    };
}

// Overall viewmodel for this screen, along with initial state
function ReservationsViewModel() {
    var self = this;

    // Editable data
    self.seats = [
        new SeatReservation("Steve", availableMeals[0]),
        new SeatReservation("Bert", availableMeals[1])
    ];

    self.numSeats = self.seats.length;

    //Because we are using functions rather than computed variables, we cannot use a computed for totalSurcharge.
    // Another option is to emit an event when we update seats and catch it here to update the total.
    var total = 0;
    total = self.seats.reduce(function(previous, currentSeat){
        return previous + currentSeat.meal.price;
    }, total);

    self.totalSurcharge = total.toFixed(2);

    self.totalDiscount = function() {
        var totalSurcharge = parseInt(this.totalSurcharge);
        return (totalSurcharge*90/100).toFixed(2);
    };

    var seatNum = function() {
        return this.seats.length;
    };

    // Operations
    self.addSeat = function() {
        self.seats.push(new SeatReservation("", availableMeals[0]));
    }
    self.removeSeat = function(e) {
        self.seats.$remove(e.targetVM.$data)
    }

    var data = {
        availableMeals: availableMeals,
        totalSurcharge: self.totalSurcharge,
        seats: self.seats,
        numSeats: self.numSeats
    }
    var computed = {
        totalDiscount: self.totalDiscount,
        seatNum: seatNum
    }

    var methods = {
        addSeat: self.addSeat,
        removeSeat: self.removeSeat
    }

    return {
        data: data,
        computed: computed,
        methods: methods
    }
}

var vm = $.extend({el: '#vue-meal-reservation'}, new ReservationsViewModel());
var vue = new Vue(vm);

//Because we are using functions rather than computed variables, we cannot use a computed for totalSurcharge.
// Another option is to emit an event when we update seats and catch it here to update the total.
vue.$watch('seats', function (value) {
    var total = 0;

    this.totalSurcharge = value.reduce(function(previous, currentSeat){
        return previous + currentSeat.meal.price;
    }, total);
});