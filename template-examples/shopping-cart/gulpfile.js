/**
 * Created by redge on 7/18/2014.
 */
var gulp = require('gulp');
var gutil = require("gulp-util");

var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var gulpWebpack = require('gulp-webpack');
var replace = require('gulp-replace');

var wpkConfig = require('./webpack.config.js');

var webpack = require("webpack");
var WebpackDevServer = require("webpack-dev-server");



gulp.task("webpack", function () {
    return gulp.src('./assets/javascripts/entry.js')
        .pipe(gulpWebpack(wpkConfig))
        .pipe(gulp.dest('./dist/bundle'));
});

gulp.task("vendor-scripts", function () {
    return gulp.src(['./bower_components/firebase/firebase.js', './bower_components/firebase-simple-login/firebase-simple-login.js'])
        .pipe(gulp.dest('./dist/bundle'));
});

// CSS concat, auto-prefix and minify
gulp.task('styles', function () {
    gutil.log("Building stylesheets");
    gulp.src(['./assets/stylesheets/*.css'])
        .pipe(concat('styles.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./dist/stylesheets'));
});

gulp.task('fonts', function () {
    gutil.log("Building stylesheets");
    gulp.src(['{location of fonts}'])
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('build', ['webpack', 'vendor-scripts', 'fonts', 'styles', 'html']);

gulp.task('html', function () {
    return gulp.src(['./views/**/*.html'], {base: '.'})
        .pipe(replace('/dist/bundle/', 'bundle/'))
        .pipe(replace('/dist/stylesheets/', 'stylesheets/'))
        .pipe(concat('index.html'))
        .pipe(gulp.dest('./dist'));
});

// default gulp task
gulp.task('default', ['build', 'watch']);

// Debug using webpack development server
gulp.task('webpack-dev', ['default'], function() {

    // modify some webpack config options
    var defaultPort = 8080;
    var config = Object.create(wpkConfig);
    config.devtool = "eval";
    config.debug = true;

    var compiler = webpack(config);

    // Start a webpack-dev-server
    new WebpackDevServer(compiler, {
        publicPath: "/" + config.output.publicPath,
        stats: {
            colors: true
        }
    }).listen(defaultPort, "localhost", function(err) {
            if(err) throw new gutil.PluginError("webpack-dev-server", err);
            gutil.log("[webpack-dev-server]", "http://localhost:8080/dist");
        });
});

// Watch for changes and build accordingly
gulp.task('watch', function() {

    // watch for JS changes
    gulp.watch('./assets/javascripts/**/*.js', function() {
        gulp.run('webpack');
    });

    // watch for CSS changes
    gulp.watch('./assets/stylesheets/**/*.css', function() {
        gulp.run('styles');
    });

    // watch for HTML changes
    gulp.watch('./views/**/*.html', function() {
        gulp.run('html');
    });
});