/**
 * Created by redge on 7/18/2014.
 * To install dependencies run:
 * npm install css-loader html-loader style-loader url-loader --save-dev
 */

var webpack = require("webpack");

module.exports = {
    entry: "./assets/javascripts/main.js",
    output: {
//        path: "{destination path here}",
        publicPath: "dist/",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.html$/, loader: "html" },
            //configure bootstrap font loaders
            {
                test: /\.gif$|\.eot$|\.jpe?g$|\.mp3$|\.png$|\.svg$|\.ttf$|\.ttf$|\.wav$|\.woff$/,
                loader: 'url-loader'
            },
            { test: /\.less$/, loader: "style!css!less" }
        ]
    },
    resolve: {
        modulesDirectories: ['node_modules', 'components/bower_components','components/vendor'],
        alias: {
            vue: 'vue/dist/vue.js',
            jquery: 'jquery/dist/jquery.js',
            knockout: 'knockout/dist/knockout.debug.js',
            htmljs: 'html.engine.js',
            views: __dirname + '/assets/views',
            templates: __dirname + '/assets/templates'
        }
    },
    amd: {
        jQuery: true

    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }) //Create global plugin
    ]
};