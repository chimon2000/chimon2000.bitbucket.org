$(document).ready(function(){
    var ryuStill = $('.ryu-still'),
        ryuReady = $('.ryu-ready'),
        ryuThrowing = $('.ryu-throwing'),
        hadouken = $('.hadouken'),
        ryuCool = $('.ryu-cool');

    var $ryu = $('.ryu');

    $ryu.on('mouseenter', function () {
        ryuStill.hide();
        ryuReady.show();
    });

    $ryu.on('mouseleave', function () {
        ryuStill.show();
        ryuReady.hide();
    });

    $ryu.on('mousedown', function () {
        playHadoukenSound();
        ryuStill.hide();
        ryuReady.hide();
        ryuThrowing.show();
        hadouken.finish().show()
            .animate(
            {'left': '1100px'},
            500,
            function () {
                $(this).hide();
                $(this).css('left', '500px');
            }
        );
    });

    $ryu.on('mouseup', function () {
        ryuThrowing.hide();
        ryuReady.show();
    });

    $('body').on('keydown', function (e) {
        if (e.which == 88) {
            ryuStill.hide();
            ryuReady.hide();
            ryuThrowing.hide();
            hadouken.hide();
            ryuCool.show();
        };
    })
        .on('keyup', function () {
            ryuCool.hide();
            ryuStill.show();
        });
});

function playHadoukenSound () {
    var hadoukenSound = $('#hadouken-sound')[0];

    hadoukenSound.volume = 0.5;
    hadoukenSound.load();
    hadoukenSound.play();
}